# boolean-expression-calc :abacus:

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/boolean-expression-calc/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/boolean-expression-calc?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/boolean-expression-calc?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/boolean-expression-calc?style=for-the-badge)

[![repl](https://repl.it/badge/github/KennyOliver/boolean-expression-calc)](https://repl.it/@KennyOliver/boolean-expression-calc)

**Calculate simple Boolean expressions!**
* AND
* OR
* XOR
* NOT

## VividHues :rainbow: :package:
**boolean-expression-calc** uses **VividHues** - my own Python Package!

[![VividHues](https://img.shields.io/badge/Get%20VividHues-252525?style=for-the-badge&logo=python&logoColor=white&link=https://github.com/KennyOliver/VividHues)](https://github.com/KennyOliver/VividHues)

---
Kenny Oliver ©2021
